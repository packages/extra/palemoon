# Maintainer: Bernhard Landauer <oberon@manjaro.org>
# latest version: https://www.palemoon.org/download.shtml

pkgname=palemoon
pkgver=33.6.0.1
pkgrel=1
pkgdesc="Open source web browser based on Firefox focusing on efficiency."
arch=('x86_64')
url="http://linux.palemoon.org/"
license=('MPL' 'GPL' 'LGPL')
depends=('alsa-lib'
    'dbus-glib'
    'desktop-file-utils'
    'gtk3'
    'libxt'
    'mime-types'
    'nss')
optdepends=('ffmpeg: record, convert, and stream audio and video')
options=('!strip')
source=("https://rm-eu.palemoon.org/release/palemoon-$pkgver.linux-x86_64-gtk3.tar.xz"{,.sig}
        palemoon.desktop)
sha256sums=('f0ab86aa5c2d2e2c15feb69d789c3251c161bf8d58680f4fce75ebc460e18217'
            'SKIP'
            '442f46eadbb9a54bfb9549d8ad81603e3c21ce7bdc9095f6a391cff992536adc')
validpgpkeys=('439F46F42C6AE3D23CF52E70865E6C87C65285EC' # trava90 <travawine@protonmail.com>>
              '3DAD8CD107197488D2A2A0BD40481E7B8FCF9CEC') # Moonchild (RSA signing key) <moonchild@palemoon.org>

package() {
  install -d "$pkgdir"/usr/{bin,lib}
  cp -r palemoon/ "$pkgdir/usr/lib/palemoon"
  ln -s ../lib/palemoon/palemoon "$pkgdir/usr/bin/palemoon"
  install -Dm644 palemoon.desktop "$pkgdir/usr/share/applications/palemoon.desktop"

  # icons
  install -Dm644 palemoon/browser/chrome/icons/default/default16.png \
    "$pkgdir/usr/share/icons/hicolor/16x16/apps/palemoon.png"
  install -Dm644 palemoon/browser/chrome/icons/default/default32.png \
    "$pkgdir/usr/share/icons/hicolor/32x32/apps/palemoon.png"
  install -Dm644 palemoon/browser/chrome/icons/default/default48.png \
    "$pkgdir/usr/share/icons/hicolor/48x48/apps/palemoon.png"
  install -Dm644 palemoon/browser/icons/mozicon128.png \
    "$pkgdir/usr/share/icons/hicolor/128x128/apps/palemoon.png"
}
